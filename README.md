# WordPress Theme for joelkrause.co

## Scripts Used
- TweenMax
- FontAwesome (I guess?)

## Plugins Used
- Google Native Lazy Load
- WPPusher
- Really Simple SSL

## Build tools
- Using gulp
- to build (and watch for changes), run `gulp watch` command