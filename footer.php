<footer class="site__footer">
    <div class="wrapper">
        &copy; Joel Krause | <a href="https://github.com/joelkrause/jkrs-wp" target="_blank">See on GitHub</a>
    </div>
</footer>
<?php wp_footer();?>
</body>

</html>